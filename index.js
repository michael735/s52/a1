function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
  
    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.

    if (letter.length == 1){
        for (i = 0 ; i < sentence.length; i++)
        {
            if (sentence.charAt(i) == letter)
            {
                 result += 1
            }

        }
        return result   
    }else{

        return undefined
    }
}


    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.


    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.


    function isIsogram(text) {


    return text.split('').filter(

        (item, pos, arr) => arr.indexOf(item) == pos).length == text.length;
        
    
}

function purchase(age, price) {

   

    //Conditions:
        // Return undefined for people aged below 13.
        if (age < 13){
            return undefined
        }
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        else if ((13 <= age && age <= 21) || age>=60){
            return (price*.80).toFixed(2)
        }
        // Return the rounded off price for people aged 22 to 64.
        else {
            return price.toFixed(2)
        }
    //Check:
        // The returned value should be a string.
    
}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    const uniqueCategory =  
    items.map(result => result.category).filter((value, index, data) => data.indexOf(value) === index); 

    return uniqueCategory

            
    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const flyingVoters = candidateA.filter(voters => candidateB.includes(voters))

    return flyingVoters
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};